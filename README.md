# Drone.io CI Docker image

The purpose of this image is to be as small as possible (186.8 MB only).
It is running the latest version of the [open source Drone.io](https://github.com/drone/drone).

Drone.io CI purpose is to build Docker images in order to run your tests.
This means that when the build pass, you have an image ready for UAT, and
production.

# Usage

## Short story

    $ sudo docker run -d --name dind --privileged -it -p 4243 -e PORT=4243 jpetazzo/dind
    $ sudo docker run -d --name drone --link dind:dind -p 80:80 zedtux/drone

Then open in your browser the URL http://\<ip-address\>/install.

## Long story

### Drone.io

On the machine where Docker is installed:

    $ sudo mkdir -p /etc/docker/drone/
    $ sudo docker pull zedux/drone
    $ sudo docker run --rm --name drone -p 80:80 -v /etc/docker/drone/:/var/lib/drone/ zedtux/drone

Then open in your browser the URL http://\<ip-address\>/install.

### Docker in Docker

Now that you have the application running, you need to have a Docker instance
for Drone.io otherwise you will have the following error:

    dial tcp 0.0.0.0:4243: connection refused

I recommend to use a `dind` image (__d__ ocker __in__ __d__ ocker) in order to be
able to update an image (Drone or dind) without shutting down the other.

    $ sudo mkdir -p /etc/docker/dind/
    $ sudo docker pull jpetazzo/dind
    $ sudo docker run -d --name dind --privileged -t -i -p 4243 -e PORT=4243 -v /etc/docker/dind:/var/lib/docker jpetazzo/dind

Now you need to link both containers. First, stop the `drone` image and start
it again with the link:

    $ sudo docker stop drone
    $ sudo docker run --rm --name drone -p 80:80 --link dind:dind -v /etc/docker/drone/:/var/lib/drone/ zedtux/drone

### My first build

Now that you have a working Drone instance, you have to follow [the Drone README](https://github.com/drone/drone)
in order to link your instance with Github and import your first repository.
